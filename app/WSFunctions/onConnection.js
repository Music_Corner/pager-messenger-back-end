const onConnection = (connection, messageHistory) => {
	const response = JSON.stringify({
		data: messageHistory,
		status: 200,
	});

	connection.send(response);
};

module.exports = onConnection;
