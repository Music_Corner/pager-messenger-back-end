const MESSAGE_TYPES = require('../Init/constants').MESSSAGE_TYPES;
const addMessageToDb = require('../httpFunctions/POST/Messages/addMessageToDb');
const getCurrentDate = require('../Init/dateFunctions');
const getUserIdByToken = require('../httpFunctions/POST/Users/getUserIdByToken');
const readMessage = require('../httpFunctions/POST/Messages/readMessage');

const onMessage = (users, connection, message, userInfo, user) => {
	let response;
	const { data, type, recieverId } = JSON.parse(message.utf8Data);

	switch (type) {
		case MESSAGE_TYPES.MESSAGE:
			response = JSON.stringify({
				data: {
					message: data.message,
					from_id: data.from_id,
					to_id: data.to_id,
					created_at: getCurrentDate(),
				},
				type,
				status: 200,
			});

			addMessageToDb(data).then((messageData) => {
				if (messageData.status === 200) {
					connection.send(response);
					users.forEach((userReciever) => {
						const isUserIdEqualsRecieverId = parseInt(user.id) === parseInt(recieverId);
						if (parseInt(userReciever.id) === parseInt(recieverId) && !isUserIdEqualsRecieverId) {
							userReciever.connection.send(response);
						}
					});
				} else {
					connection.send(JSON.stringify(messageData));
				}
			});

			break;

		case MESSAGE_TYPES.INIT_USER_CONNECTION:
			console.log(`init user id: ${userInfo.id}`);
			user.id = userInfo.id;

			break;

		case MESSAGE_TYPES.READ_MESSAGES:
			getUserIdByToken(data.token).then(userId => {
				if (userId) {
					readMessage(userId, recieverId).then(() => {
						response = JSON.stringify({
							type,
							status: 200,
							data: {
								to_id: recieverId,
							},
						});
						users.forEach((userReciever) => {
							const isUserIdEqualsRecieverId = parseInt(user.id) === parseInt(recieverId);
							if (parseInt(userReciever.id) === parseInt(recieverId) && !isUserIdEqualsRecieverId) {
								userReciever.connection.send(response);
							}
						});
					});
				}
			});

			break;

		default:
			break;
	}
};

module.exports = onMessage;
