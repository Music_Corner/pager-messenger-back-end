const mySqlConnection = require('../../../Other/mySql/connection');
// const getCurrentDate = require('../../../Init/dateFunctions');
const { RESPONSES } = require('../../../Init/constants');
const checkUsersExistanceById = require('../Users/checkUsersExistanceById');

const addMessageToDb = message => (
	new Promise(resolve => {
		checkUsersExistanceById(message.to_id).then((existance) => {
			if (existance) {
				mySqlConnection.query(`INSERT INTO messages_content (content) VALUES ('${message.message}')`, (contentError) => {
					if (!contentError) {
						// const createdAtDate = getCurrentDate();
						mySqlConnection.query(`INSERT INTO messages (from_id, to_id, content_id, created_at) VALUES(${`${message.from_id}, ${message.to_id}, LAST_INSERT_ID(), NOW()`})`, (messagesInsertError) => {
							if (!messagesInsertError) {
								resolve({ status: 200 });
							} else {
								console.log(messagesInsertError);
							}
						});
					} else {
						console.log(contentError);
					}
				});
			} else {
				resolve(RESPONSES.SEND_MESSAGE_USER_ID_ERROR);
			}
		});
	})
);

module.exports = addMessageToDb;
