const mysqlConnection = require('../../../Other/mySql/connection');
const checkUsersExistanceById = require('../Users/checkUsersExistanceById');
const { RESPONSES } = require('../../../Init/constants');

const getMessages = (userId, collocutorId) => {
	return new Promise((resolve) => {
		checkUsersExistanceById(collocutorId).then((existance) => {
			if (existance) {
				const finalMessages = [];
				mysqlConnection.query('SELECT * FROM messages_content', (messageContentError, content) => {
					if (messageContentError) {
						resolve(RESPONSES.GET_ERROR_RESPONSE(messageContentError));
					}

					mysqlConnection.query(`SELECT * FROM messages WHERE from_id = ${userId} AND to_id = ${collocutorId} OR from_id = ${collocutorId} AND to_id = ${userId}`, (error, clientMessages) => {
						if (error) {
							resolve(RESPONSES.GET_ERROR_RESPONSE(error));
						}

						content.forEach((contentObject) => {
							clientMessages.forEach((message) => {
								if (contentObject.id === message.content_id) {
									finalMessages.push({
										...message,
										message: contentObject.content,
									});
								}
							});
						});
						resolve(RESPONSES.GET_SUCCESS_RESPONSE(finalMessages));
					});
				});
			} else {
				resolve(RESPONSES.SEND_MESSAGE_USER_ID_ERROR);
			}
		});
	});
};

module.exports = getMessages;
