const mySqlConnection = require('../../../Other/mySql/connection');
const { RESPONSES } = require('../../../Init/constants');

const getUniqueArray = (arr, comp) => {
	const unique = arr
		.map(e => e[comp])

		// store the keys of the unique objects
		.map((e, i, final) => final.indexOf(e) === i && i)

		// eliminate the dead keys & store unique objects
		.filter(e => arr[e]).map(e => arr[e]);

	return unique;
};

const getDialogsList = (userId) => {
	return new Promise((resolve) => {
		mySqlConnection.query(`SELECT * FROM messages WHERE from_id = ${userId} OR to_id = ${userId} ORDER BY created_at DESC`, (messagesFromUserErr, messagesOfUser) => {
			if (messagesFromUserErr) {
				resolve(RESPONSES.GET_ERROR_RESPONSE(messagesFromUserErr));
			} else {
				mySqlConnection.query('SELECT * FROM messages_content', (messageContentError, messagesContent) => {
					if (messageContentError) {
						resolve(RESPONSES.GET_ERROR_RESPONSE(messageContentError));
					} else {
						const userMessages = messagesContent.map((messageContent) => {
							let message = {};
							messagesOfUser.find((messageOfUser) => {
								if (messageContent.id === messageOfUser.content_id) {
									message = { ...messageOfUser, content: messageContent.content };
									return true;
								}
							});
							return message;
						}).filter(filteredMessage => (
							Object.keys(filteredMessage).length > 0 && filteredMessage
						));

						const uniqueMessagesFromId = getUniqueArray(userMessages.map(message => (message.to_id === userId && message)).reverse(), 'from_id');
						const uniqueMessagesToId = getUniqueArray(userMessages.map(message => (message.from_id === userId && message)).reverse(), 'to_id');

						const dialogsList = [];
						uniqueMessagesFromId.forEach((uniqueMessage) => {
							let finalMessage = uniqueMessage;
							uniqueMessagesToId.find((message) => {
								if (message.to_id === uniqueMessage.from_id) {
									finalMessage = message.created_at > uniqueMessage.created_at
										? message : uniqueMessage;
									return message.to_id === uniqueMessage.from_id;
								}
							});

							dialogsList.push(finalMessage);
						});

						uniqueMessagesToId.forEach((uniqueMessage) => {
							let finalMessage = uniqueMessage;
							uniqueMessagesFromId.find((message) => {
								if (message.from_id === uniqueMessage.to_id) {
									finalMessage = null;
									return message.from_id === uniqueMessage.to_id;
								}
							});

							if (finalMessage) {
								dialogsList.push(finalMessage);
							}
						});

						resolve(RESPONSES.GET_SUCCESS_RESPONSE(dialogsList));
					}
				});
			}
		});
	});
};

module.exports = getDialogsList;
