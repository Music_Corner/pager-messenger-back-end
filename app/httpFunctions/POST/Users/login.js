const crypto = require('crypto');
const mysqlConnection = require('../../../Other/mySql/connection');
const { RESPONSES } = require('../../../Init/constants');

const login = (body) => {
	return new Promise((resolve) => {
		mysqlConnection.query('SELECT * FROM users', (error, results) => {
			if (error) {
				resolve(RESPONSES.GET_ERROR_RESPONSE(error));
			}

			let checkedUser;

			results.forEach((user) => {
				const hashedBodyPassword = crypto.createHash('sha256').update(body.password).digest('hex');

				if (user.login === body.login && user.password === hashedBodyPassword) {
					checkedUser = user;
				}
			});

			if (checkedUser) {
				crypto.randomBytes(48, (err, buffer) => {
					const token = buffer.toString('hex');

					mysqlConnection.query(`INSERT INTO tokens (token, user_id) VALUES ('${token}', ${checkedUser.id})`, () => {});
					mysqlConnection.query(`SELECT * FROM users_info WHERE user_id = ${checkedUser.id}`, (userInfoError, userInfoResults) => {
						if (error) {
							resolve(RESPONSES.GET_ERROR_RESPONSE(userInfoError));
						}

						const userData = {
							id: checkedUser.id,
							...userInfoResults[0],
							token,
						};

						resolve(RESPONSES.GET_SUCCESS_RESPONSE(userData));
					});
				});
			} else {
				resolve(RESPONSES.LOGIN_FORM_ERROR);
			}
		});
	});
};

module.exports = login;
