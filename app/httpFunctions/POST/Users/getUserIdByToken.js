const mySqlConnection = require('../../../Other/mySql/connection');

const getUserIdByToken = token => (
	new Promise((resolve) => {
		if (!token) {
			resolve(null);
		} else {
			mySqlConnection.query(`SELECT user_id FROM tokens WHERE token = '${token}'`, (error, usersIds) => {
				if (error) {
					resolve(null);
				} else if (usersIds.length > 0) {
					resolve(usersIds[0].user_id);
				}

				resolve(null);
			});
		}
	})
);

module.exports = getUserIdByToken;
