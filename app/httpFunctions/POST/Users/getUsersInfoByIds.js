const mySqlConnection = require('../../../Other/mySql/connection');
const { RESPONSES } = require('../../../Init/constants');

const getUserInfoById = (usersIds) => {
	return new Promise((resolve) => {
		mySqlConnection.query('SELECT * FROM users_info', (queryError, results) => {
			if (queryError) {
				resolve(RESPONSES.GET_ERROR_RESPONSE(queryError));
			} else {
				const users = usersIds.map(userId => (
					results.find(user => (
						parseInt(userId) === parseInt(user.user_id) && user
					))
				));
				resolve(RESPONSES.GET_SUCCESS_RESPONSE(users));
			}
		});
	});
};

module.exports = getUserInfoById;
