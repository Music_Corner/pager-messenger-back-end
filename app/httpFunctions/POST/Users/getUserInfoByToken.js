const mysqlConnection = require('../../../Other/mySql/connection');
const getUserIdByToken = require('./getUserIdByToken');

const { RESPONSES } = require('../../../Init/constants');

const getUserInfoByToken = token => (
	new Promise((resolve) => {
		getUserIdByToken(token).then((userId) => {
			if (!userId) {
				resolve(RESPONSES.LOGIN_ERROR);
			} else {
				mysqlConnection.query(`SELECT * FROM users_info WHERE user_id = ${userId}`, (userInfoError, userInfoResults) => {
					if (userInfoError) {
						resolve(RESPONSES.GET_ERROR_RESPONSE(userInfoError));
					} else if (userInfoResults.length === 1) {
						const userData = {
							id: userInfoResults[0].user_id,
							...userInfoResults[0],
							token,
						};

						resolve(RESPONSES.GET_SUCCESS_RESPONSE(userData));
					} else {
						resolve(RESPONSES.LOGIN_ERROR);
					}
				});
			}
		});
	})
);

module.exports = getUserInfoByToken;
