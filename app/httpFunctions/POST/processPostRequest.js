const { POST_REQUEST_URLS, RESPONSES } = require('../../Init/constants');

const login = require('./Users/login');
const getUserInfoByToken = require('./Users/getUserInfoByToken');
const getUserIdByToken = require('./Users/getUserIdByToken');
const getMessages = require('./Messages/getMessages');
const getDialogsList = require('./Messages/getDialogsList');
const getUsersInfoByIds = require('./Users/getUsersInfoByIds');


const processPostRequest = (request, response) => {
	const body = [];
	request.on('data', (chunk) => {
		body.push(chunk);
	});

	let requestCallBack = (parsedBody) => {};

	const successCallBack = (data) => {
		response.writeHead(data.status);
		response.end(JSON.stringify(data));
	};

	const loginErrorCallBack = () => {
		response.writeHead(400);
		response.end(JSON.stringify(RESPONSES.LOGIN_ERROR));
	};

	switch (request.url) {
		case POST_REQUEST_URLS.LOGIN:
			requestCallBack = (parsedBody) => {
				login(parsedBody).then(successCallBack);
			};

			break;

		case POST_REQUEST_URLS.CHECK_LOGIN:
			requestCallBack = (parsedBody) => {
				getUserInfoByToken(parsedBody.token).then(successCallBack);
			};

			break;

		case POST_REQUEST_URLS.GET_MESSAGES:
			requestCallBack = (parsedBody) => {
				getUserIdByToken(parsedBody.token).then((userId) => {
					if (userId) {
						getMessages(userId, parsedBody.collocutorId).then(successCallBack);
					} else {
						loginErrorCallBack();
					}
				});
			};

			break;

		case POST_REQUEST_URLS.GET_MESSAGES_LIST:
			requestCallBack = (parsedBody) => {
				getUserIdByToken(parsedBody.token).then((userId) => {
					if (userId) {
						getDialogsList(userId).then(successCallBack);
					} else {
						loginErrorCallBack();
					}
				});
			};

			break;

		case POST_REQUEST_URLS.GET_USER_INFO:
			requestCallBack = (parsedBody) => {
				getUserIdByToken(parsedBody.token).then((userId) => {
					if (userId) {
						getUsersInfoByIds(parsedBody.usersIds).then(successCallBack);
					} else {
						loginErrorCallBack();
					}
				});
			};

			break;

		default:
			response.writeHead(400);
			response.end(JSON.stringify(RESPONSES.METHOD_NOT_FOUND));

			break;
	}

	request.on('end', () => {
		const parsedBody = JSON.parse(body);
		requestCallBack(parsedBody);
	});
};

module.exports = processPostRequest;
