const getPages = (request, response) => {
	const url = require('url');
	const fs = require('fs');
	const path = require('path');

	const baseDirectory = '../../'; // or whatever base directory you want
	const requestUrl = url.parse(request.url);
	const fsPath = baseDirectory + path.normalize(requestUrl.pathname);

	const fileStream = fs.createReadStream(fsPath);
	fileStream.pipe(response);
	fileStream.on('open', () => {
		response.writeHead(200);
	});

	fileStream.on('error', () => {
		response.writeHead(404); // assume the file doesn't exist
		response.end(JSON.stringify({ status: 404, error: 'Not Found' }));
	});
};

module.exports = processGetRequest;
