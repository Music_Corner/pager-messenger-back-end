const mysql = require('mysql');

const config = {
	host: '127.0.0.1',
	port: 3306,
	user: 'root',
	password: 'password',
	database: 'chat-db',
};

const mysqlConnection = mysql.createConnection(config);

module.exports = mysqlConnection;
