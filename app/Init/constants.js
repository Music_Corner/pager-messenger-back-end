const MESSSAGE_TYPES = {
	MESSAGE: 'MESSAGE',
	INIT_USER_CONNECTION: 'INIT_USER_CONNECTION',
	GET_MESSAGES_LIST: 'GET_MESSAGES_LIST',
	READ_MESSAGES: 'READ_MESSAGES',
};

const POST_REQUEST_URLS = {
	LOGIN: '/login',
	CHECK_LOGIN: '/checkLogin',
	GET_MESSAGES: '/getMessages',
	GET_MESSAGES_LIST: '/getMessagesList',
	GET_USER_INFO: '/getUserInfo',
};

const RESPONSES = {
	LOGIN_FORM_ERROR: { status: 400, error: 'Not valid login or password' },
	LOGIN_ERROR: { status: 400, error: 'You are not logged in!' },
	DB_QUERY_ERROR: { status: 400, error: 'Db query error...' },
	SEND_MESSAGE_USER_ID_ERROR: { status: 400, error: 'There is no user you are trying to message' },
	SOMETHING_WENT_WRONG_ERROR: { status: 400, error: 'Somethin went wrong...' },
	METHOD_NOT_FOUND: { status: 400, error: 'Method not found' },
	GET_SUCCESS_RESPONSE: data => ({ status: 200, data }),
	GET_ERROR_RESPONSE: error => ({ status: 500, error: error.code }),
};

module.exports = { MESSSAGE_TYPES, POST_REQUEST_URLS, RESPONSES };
