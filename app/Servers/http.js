const http = require('http');
// const processGetRequest = require('../httpFunctions/GET/processGetRequest');
const processPostRequest = require('../httpFunctions/POST/processPostRequest');

const port = 9615;

const httpServer = http.createServer((request, response) => {
	response.setHeader('Access-Control-Allow-Origin', '*');
	response.setHeader('Access-Control-Allow-Headers', 'origin, content-type, accept');

	try {
		if (request.method === 'POST') {
			processPostRequest(request, response);
		} else {
			// processGetRequest(request, response);
		}
	} catch (e) {
		response.writeHead(500);
		response.end(JSON.stringify({ status: 500, error: 'Something went wrong...' })); // end the response so browsers don't hang
	}
}).listen(port);

console.log(`listening on port ${port}`);

module.exports = httpServer;
