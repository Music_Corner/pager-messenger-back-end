const WebSocketServer = require('websocket').server;

const onMessage = require('../WSFunctions/onMessage');
const httpServer = require('./http');
const getUserInfoByToken = require('../httpFunctions/POST/Users/getUserInfoByToken');

const socketServer = () => {
	const users = [];
	const wsServer = new WebSocketServer({ httpServer });

	wsServer.on('request', (request) => {
		console.log(`opened, user-key: ${request.key}`);
		const connection = request.accept(null, request.origin);

		users.push({
			connection,
			key: request.key,
			id: null,
		});

		const user = users[users.length - 1];

		connection.on('message', (message) => {
			if (message.type === 'utf8') {
				const parsedMessage = JSON.parse(message.utf8Data).data;

				getUserInfoByToken(parsedMessage.token).then((userInfo) => {
					if (userInfo.status === 200) {
						onMessage(users, connection, message, userInfo.data, user);
					} else {
						connection.send(JSON.stringify({ status: 400, error: 'You are not logged in!' }));
					}
				});
			}
		});

		connection.on('close', (code, desc) => {
			console.log(`closed code: ${code} description: ${desc}`);
		});
	});
};

module.exports = socketServer;
